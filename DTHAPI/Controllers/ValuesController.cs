﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DTHAPI.Data;
using Microsoft.EntityFrameworkCore;

namespace DTHAPI.Controllers
{
    // http://localhost:5000/api/values
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly DataContext _context;
        public ValuesController(DataContext context)
        {
            _context = context;
        }
        // GET api/values
        // [HttpGet]
        // public async Task<IActionResult> GetValues()
        // {
        //     var movie = await _context.Movies.ToListAsync();
        //     return Ok(movie);
        // }

        // // GET api/values/5
        // [HttpGet("{id}")]
        // public async Task<IActionResult> GetValues(int id)
        // {
        //     var movie = await _context.Movies.Where(m => m.Id == id).FirstOrDefaultAsync();
        //     return Ok(movie);
        // }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetGenres()
        {
            var genre = await _context.Genres.ToListAsync();
            return Ok(genre);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGenres(int id)
        {
            var genre = await _context.Genres.Where(m => m.Id == id).FirstOrDefaultAsync();
            return Ok(genre);
        }
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
